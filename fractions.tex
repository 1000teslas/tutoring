\documentclass[12pt]{scrartcl}
\usepackage{mathtools, amsthm, amssymb, siunitx}
\usepackage[colorlinks=true]{hyperref}

\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\mathtoolsset{showonlyrefs}

\theoremstyle{plain}
\newtheorem{thm}[subsubsection]{Theorem}
\newtheorem{lem}[subsubsection]{Lemma}
\theoremstyle{definition}
\newtheorem{defn}[subsubsection]{Definition}
\theoremstyle{remark}
\newtheorem{rem}[subsubsection]{Remark}
\newtheorem{ex}[subsubsection]{Example}

\sisetup{quotient-mode=fraction}

\title{Fractions}
\author{Kevin Tran}
\begin{document}
\maketitle

\section{Why the need for all these definitions?}
\label{sec:definitions}
In mathematics, to be sure of our conclusions, we must reason logically. To
reason logically, we must be sure where we are starting from. These definitions
are the starting point, and must be precise to minimize misunderstandings.

\section{The number line}
\label{sec:number line}
On a horizontal line, pick a point, called 0, and a point to right of 0, called
1, or the unit. By copying this length, but with 1 as the left endpoint, we
obtain 2 on the right endpoint. Repeating this process, we obtain an infinite
sequence of equally spaced points called the whole numbers,
\(\mathbb{N}=\{0,1,2,\ldots\}\).

\begin{defn}
  We call the line, along with a sequence of points \(\mathbb{N}\),
  constructed as above, as the number line. A number is the point on the number
  line.
\end{defn}
\begin{rem}
  If we choose different points to be 0 and 1, we will end up with a different
  number line. Hence it is impossible to say which fraction a point represents
  unless the location of 1 is specified.
\end{rem}
\begin{rem}
  The interpretation of a fraction depends on the meaning of the unit. If the
  unit is the volume of a bottle, then the meaning of \(\frac{1}{3}\) on this
  number line is a third of the volume of the bottle. If the unit is the height
  of liquid in a bottle, then the meaning of \(\frac{1}{3}\) on this number line
  is the third of the height of liquid in the bottle. The fraction
  \(\frac{1}{3}\) by itself has no meaning other than being the 1st point in
  the sequence of 3rds.
\end{rem}

\begin{defn}
  Given numbers \(a\) and \(b\), the segment between \(a\) and \(b\),
  inclusive of the endpoints \(a\) and \(b\), is denoted by \([a,b]\).
\end{defn}

\begin{defn}
  Segments \([a,b]\) and \([c,d]\), have equal length or the same length, if by
  sliding \([a,b]\) along the number line such that \(a\) is at \(c\), then
  \(b\) is at \(d\).
\end{defn}
\begin{rem}
  One way of defining 2 is the number such that \([1,2]\) and \([0,1]\) have
  equal length.
\end{rem}

\begin{defn}
  If segments \([a,b]\) and \([0,k]\) have the same length, then \([a,b]\)
  has length \(k\).
\end{defn}
\begin{rem}
  One way of defining 3 is the number such that \([2,3]\) has length \(1\).
\end{rem}

\begin{defn}
  A sequence of points is equispaced or equidistant if the segments between
  any 2 consecutive points have the same length.
\end{defn}

\begin{rem}
  Why do we use the number line? Suppose we have a pizza with 8 slices, and we
  divide the pizza among 2 people. This represents \(8\div 2\), which can be
  done by counting to get 2 equal groups. But what about \(8\div 3\)? It is not
  obvious how we can divide 8 slices of pizza between 3 people, but it is clear
  that we can divide \([0,8]\) into 3 equal parts.
\end{rem}

\section{What are fractions?}
\label{sec:fractions}
\begin{defn}
  For any nonzero whole number \(n\), a segment \([a,b]\) is divided into \(n\)
  equal parts by a sequence of \(n-1\) points \(p_1,\ldots,p_{n-1}\) if the
  sequence \(a,p_1,\ldots,p_{n-1},b\) is equispaced.
\end{defn}
\begin{rem}
  For all segments \([a,b]\), there always exists \(n-1\) such points.
\end{rem}

\begin{defn}
  Divide each of the segments \([0,1],[1,2],\ldots\) into \(n\) equal parts.
  Counting each of the dividing points and the whole numbers from left to right
  gives us the sequence of \(n\)-ths.
\end{defn}

\begin{defn}
  For any nonzero whole number \(n\), an element of the sequence of \(n\)-ths is
  called a fraction.
\end{defn}

\begin{defn}
  The set of all sequences of \(n\)-ths, over all nonzero whole numbers \(n\),
  is called the fractions.
\end{defn}

\begin{defn}
  We define \(\frac{0}{n}\) as 0, and for any nonzero whole number \(m\),
  \(\frac{m}{n}\) is the \(m\)-th point in the sequence of \(n\)-ths to the
  right of 0, or the \(m\)-th multiple of \(\frac{1}{n}\).
\end{defn}

\begin{defn}
  Given a fraction \(\frac{m}{n}\), \(m\) is called the numerator of the
  fraction and \(n\) the denominator of the fraction.
\end{defn}
\begin{rem}
  We can identify a fraction \(\frac{m}{n}\) as the segment \([0,\frac{m}{n}]\),
  or really any segment of length \(\frac{m}{n}\), say
  \([\frac{9}{n},\frac{m+9}{n}]\). It is convenient, however, to identify the
  fraction \(\frac{m}{n}\) as the segment with starting point 0, that is,
  \([0,\frac{m}{n}]\), or even just the right endpoint.
\end{rem}

\begin{defn}
  A fraction \(\frac{1}{n}\) with numerator equal to 1, is called a unit fraction.
\end{defn}

\begin{rem}
  The unit fraction \(\frac{1}{n}\) plays a similar role in the sequence of
  \(n\)-ths as 1 does in the whole numbers.
\end{rem}

\begin{defn}
  Let \(L\) and \(L'\) be segments on the number line. Then the concatenation of
  \(L\) and \(L'\) is the line segment formed by sliding the left endpoint of
  \(L'\) over to the right endpoint of \(L\), and joining the line segments.
\end{defn}
\begin{ex}
  If \(L\) is the segment \([0,\frac{1}{2}]\) and \(L'\) is the segment
  \([\frac{1}{2},1]\), then the concatenation of \(L\) and \(L'\) is the segment
  \([0,1]\).
\end{ex}
\begin{ex}
  By concatenating the 4 segments
  \([0,\frac{1}{3}],[\frac{1}{3},\frac{2}{3}],[\frac{2}{3},\frac{3}{3}],[\frac{3}{3},\frac{4}{3}]\),
  we obtain the segment \([0,\frac{4}{3}]\).
\end{ex}
\begin{defn}
  By concatenating the \(m\) segments
  \([0,\frac{1}{n}], [\frac{1}{n},\frac{2}{n}], \ldots,
  [\frac{m-1}{n},\frac{m}{n}]\), we obtain the segment \([0,\frac{m}{n}]\).
  These segments each have length \(\frac{1}{n}\), and so we say \(\frac{m}{n}\)
  is \(m\) copies of \(\frac{1}{n}\).
\end{defn}
\begin{defn}
  Two fractions \(A\) and \(B\) are equal if they represent the same
  point on the number line, and is denoted by \(A=B\).
\end{defn}
\begin{defn}
  A whole number \(m\) is less than a whole number \(n\) if \(m\) comes before
  \(n\) when counting, or \(m\) is to the left of \(n\) on the number line.
  A fraction \(A\) is less/smaller than another fraction \(B\) if \(A\) is to
  the left of \(B\) on the number line. This is denoted by \(A<B\).
\end{defn}
\begin{defn}
Finite decimals are fractions with denominator a power of 10.
\end{defn}
\begin{ex}
  \[1.23 = \frac{123}{100} = \frac{123}{10^2}.\]
\end{ex}

\begin{rem}
  Why do we define fractions this way? You might know of fractions as ``parts of
  a whole''. The ``whole'' is rather vague, at times being an area, or a slice
  of pizza, or an amount of water in a cup. All these many faces of the whole
  can be thought of as the length of some segment on the number line, and any
  time you need to think about fractions, you may refer to the number line.
  Along with fractions, you may have been introduced to the concepts of ratio,
  division, and decimals. We will not start by introducing these concepts.
  Rather, we will start with fractions, then develop these concepts from
  fractions.
\end{rem}

\section{Division}
\label{sec:division}
\begin{defn}
  Let \(m\) be a whole number, and \(n\) be a nonzero whole number. The division
  \(m\div n\) is the length of one part when a segment of length \(m\) is
  divided into \(n\) equal parts.
\end{defn}
\begin{rem}
  By definition, \(1\div n=\frac{1}{n}\).
\end{rem}
\begin{ex}
  For a division like \(12\div 3\), we can obtain the answer by counting. We
  show that the new definition agrees with the answer obtained by counting.
  Consider a segment of length 12. We can divide this segment into 12 parts of
  length 1. By the definition in terms of counting, we can divide the 12 parts
  into 3 groups of 4 parts. By concatenating 4 parts, we obtain a segment of
  length 4. These segments of length 4 divide a segment of length 12 into 3
  equal parts, so \(12\div 3=4\).
\end{ex}
\begin{ex}
  We show that \(\frac{5}{4}=5\div 4\). Consider the segment \([0,5]\). By
  dividing each of the 5 segments \([0,1],\ldots,[4,5]\) into 4 equal parts, we
  obtain 20 segments of length \(\frac{1}{4}\). By concatenating 5 segments, we
  obtain a new segment of length \(\frac{5}{4}\). Since we have 20 segments, we
  can form 4 of these new segments. Since the 20 segments were formed originally
  by dividing \([0,5]\), by joining the 4 new segments, we obtain \([0,5]\). In
  other words, we have divided \([0,5]\) into 4 equal parts, so \(5\div
  4=\frac{5}{4}\).
\end{ex}
We may apply a similar argument to obtain the following theorem:
\begin{thm}
\label{div}
  Let \(m\) be a whole number, and \(n\) be a nonzero whole number. Then
  \[\frac{m}{n}=m\div n.\]
\end{thm}
\begin{proof}
  Consider \([0,m]\) as \(m\) copies of \([0,1]\). Divide each copy of \([0,1]\)
  into \(n\) equal parts. We obtain \(mn\) segments of length \(\frac{1}{n}\).
  Divide the \(mn\) segments into \(n\) equal groups, and note that \(m\div n\)
  is the length of a segment formed by concatenating the segments in a group.
  Since each group has \(m\) segments, \(m\div n=\frac{m}{n}\).
\end{proof}
\begin{rem}
  From now on, we will not need to distinguish between \(m\div n\) and
  \(\frac{m}{n}\), and we will stop using the symbol ``\(\div\)''.
\end{rem}

\section{Comparing fractions}
\label{sec:comparing}
\begin{ex}
  Why is \(\frac{4}{3}=\frac{20}{15}\)?

  Divide the segments \([0,\frac{1}{3}], [\frac{1}{3},\frac{2}{3}], \ldots\)
  into 5 equal parts to obtain the sequence of 15ths. Since \(\frac{4}{3}\) is
  the 4th element in the sequence of 3rds, and each segment
  \([0,\frac{1}{3}],[\frac{1}{3},\frac{2}{3}],\ldots\) has been divided into 5
  equal parts by the sequence of 15ths, we see that \(\frac{4}{3}\) is the 20th
  element in the sequence of \(15\)ths. Hence \(\frac{4}{3}=\frac{20}{15}\).
\end{ex}
\begin{lem}[Equivalent fractions]
  \label{equivalent fractions} 
  Let \(\frac{m}{n}\) and \(\frac{k}{l}\) be fractions. Suppose there exists a
  nonzero whole number \(c\) such that \(k=cm\) and \(l=cn\).
  Then \[\frac{m}{n}=\frac{k}{l}.\]
\end{lem}
\begin{proof}
  Divide the segments \([0,\frac{1}{n}],[\frac{1}{n},\frac{2}{n}],\ldots\) into
  \(c\) equal parts to obtain the sequence of \(cn\)-ths. Since \(\frac{m}{n}\)
  is the \(m\)-th element in the sequence of \(n\)-ths, and each segment
  \([0,\frac{1}{n}],[\frac{1}{n},\frac{2}{n}]\ldots\) has been divided into
  \(c\) equal parts by the sequence of \(cn\)-ths, we see that \(\frac{m}{n}\)
  is the \(cm\)-th element in the sequence of \(cn\)-ths, in other words,
  \(\frac{cm}{cn}\).
\end{proof}
\begin{rem}
  Alternatively, we may phrase Lemma \ref{equivalent fractions} as: Let
  \(\frac{k}{l}\) be a fraction, and suppose there exists a whole number \(c\)
  such that \(c\) divides \(k\) and \(l\). Then \[\frac{k}{l}=\frac{k\div
      c}{l\div c}.\]
\end{rem}
\begin{ex}
  Are \(\frac{7}{9}\) and \(\frac{4}{5}\) equal? If not, which of the two is
  smaller? Note that \(\frac{7}{9}=\frac{35}{45}\) and
  \(\frac{4}{5}=\frac{36}{45}\). Clearly the numbers are not equal. It is
  tempting to then say: since \(35<36\), \(\frac{35}{45}<\frac{36}{45}\). We
  must be more careful with our reasoning, even though we have obtained the
  correct conclusion. By applying similar reasoning, one might conclude that
  since \(2<3\), \(\frac{1}{2}<\frac{1}{3}\).

  Let us provide a more precise argument. Because we count the points in the
  sequence of 45ths from left to right, and 35 comes before 36 when counting,
  the 35th point in the sequence of 45ths is to the left of the 36th point in
  the sequence of 45ths. Hence \(\frac{35}{45}<\frac{36}{45}\).
\end{ex}

\begin{thm}[Cross multiplication]
\label{fraceq}
  Let \(\frac{m}{n}\) and \(\frac{k}{l}\) be fractions.
  \begin{enumerate}
  \item If \(ml=kn\), then \(\frac{m}{n}=\frac{k}{l}\).
  \item If \(\frac{m}{n}=\frac{k}{l}\), then \(ml=kn\).
  \end{enumerate}
\end{thm}
\begin{proof}
  Note that
  \begin{equation}
    \frac{m}{n}=\frac{lm}{ln}=\frac{ml}{ln}  \quad\text{and}\quad  \frac{k}{l}=\frac{nk}{nl}=\frac{kn}{ln}.
  \end{equation}
  \begin{enumerate}
  \item\label{fraceq1} If \(ml=kn\), then \(\frac{m}{n}\) and \(\frac{k}{l}\) are the \(ml\)-th
    point in the sequence of \(ln\)-ths. Hence \(\frac{m}{n}\) and
    \(\frac{k}{l}\) are the same point, so \(\frac{m}{n}=\frac{k}{l}\).
  \item\label{fraceq2} If \(\frac{m}{n}=\frac{k}{l}\), then \(\frac{ml}{ln}\)
    and \(\frac{kn}{ln}\) are at the same position in the sequence of
    \(ln\)-ths. Hence \(ml=kn\).
  \end{enumerate}
\end{proof}
\begin{rem}
  Statement \ref{fraceq2} from above is called the converse of statement
  \ref{fraceq1}. Statement \ref{fraceq2} can be rephrased as: \(ml=kn\) only if
  \(\frac{m}{n}=\frac{k}{l}\). Theorem \ref{fraceq} can be rephrased as:
  \(ml=kn\) if and only if \(\frac{m}{n}=\frac{k}{l}\).
\end{rem}

\begin{thm}[Cross multiplication]
 \label{fraccmp}
  Let \(\frac{m}{n}\) and \(\frac{k}{l}\) be fractions. Then \(\frac{m}{n} <
  \frac{k}{l}\) if and only if \(mn<kl\).
\end{thm}
\begin{proof}
  Note that
  \begin{equation}
    \frac{m}{n}=\frac{lm}{ln}=\frac{ml}{ln}  \quad\text{and}\quad  \frac{k}{l}=\frac{nk}{nl}=\frac{kn}{ln}.
  \end{equation}
  If \(ml<kn\), then \(ml\) comes before \(kn\) when counting. Since we count
  the points in the sequence of \(ml\)-ths from left to right, the \(ml\)-th
  point is to the left of the \(kn\)-th point. Hence
  \(\frac{ml}{ln}<\frac{kn}{ln}\), and so \(\frac{m}{n} < \frac{k}{l}\).

  Conversely, if \(\frac{m}{n} < \frac{k}{l}\), then in the sequence of
  \(ln\)-ths the \(ml\)-th point must be to the left of the \(kn\)-th point.
  Since we count the points in the sequence of \(ln\)-ths from left to right,
  \(ml<kn\).
\end{proof}
\begin{rem}
  In the proof of Theorem \ref{fraceq} and \ref{fraccmp} we used the equalities
  \(\frac{m}{n}=\frac{ml}{ln}\) and \(\frac{k}{l}=\frac{kn}{ln}\). Note that
  these fractions have the same denominator. Since \(\frac{m}{n}\) and
  \(\frac{k}{l}\) are arbitrary fractions, any two fractions can be represented
  as fractions with the same denominator. We call this the fundamental fact of
  fraction-pairs.
\end{rem}

\section{What does \(\frac{2}{3}\) of something mean?}
\label{sec:math of 'of'}
When we say \(\frac{2}{3}\) of something, we are always implicitly referring to
a number of some sort. What is \(\frac{2}{3}\) of a pizza? You most likely are
thinking of something like considering the pizza as a disk, and cutting the disk
into 3 parts of equal area, then taking 2 of the parts. The number in this case
is the area of the disk. We shall define what \(\frac{2}{3}\) of a number is, or
more generally, for any nonzero fraction \(\frac{k}{l}\), what \(\frac{k}{l}\)
of a number is.
\begin{defn}
  Let \(\frac{k}{l}\) be a nonzero fraction, and \(x\) be a number on the number
  line. Then \(\frac{k}{l}\) of \(x\) is the length of \(k\) concatenated parts
  when \([0,x]\) is divided into \(l\) equal parts.
\end{defn}
\begin{rem}
  Let \(m\) and \(n\) be whole numbers. Then \(m\div n\) = \(\frac{1}{n}\) of \(m\).
\end{rem}

\begin{ex}
  What is \(\frac{1}{3}\) of \(\frac{24}{7}\)?

  Note that the sequence of 7ths divides \([0,\frac{24}{7}]\) into 24 equal
  parts. Since \(8\times 3=24\), we divide \([0,\frac{24}{7}]\) into 3 equal
  parts by marking every 8th point in the sequence of 7ths, starting from 0. We
  obtain the segments \([0,\frac{8}{7}]\), \([\frac{8}{7}, \frac{16}{7}]\), and
  \([\frac{16}{7}, \frac{24}{7}]\). To get \(\frac{1}{3}\) of \(\frac{24}{7}\),
  we consider the length of 1 of these segments, which is \(\frac{8}{7}\).
\end{ex}
\begin{ex}
  What is \(\frac{2}{5}\) of \(\frac{15}{7}\)?

  Note that \(\frac{15}{7}\) is 15 copies of \(\frac{1}{7}\). Since \(15\div
  5=3\), by dividing \(\frac{15}{7}\) into 3 equal parts, we get 3 segments,
  each consisting of 5 copies of \(\frac{1}{7}\). By taking 2 of the segments
  and concatenating them, we get 10 copies of \(\frac{1}{7}\). Hence
  \(\frac{2}{3}\) of \(\frac{15}{7}\) is \(\frac{10}{7}\).
\end{ex}
\begin{ex}
  What is \(\frac{2}{5}\) of \(\frac{8}{7}\)?

  If we try the same thing as above, this time, we will fail, since 5 does not
  divide 8 into whole numbers. By equivalent fractions, we have
  \(\frac{8}{7}=\frac{5\times 8}{5\times 7}=\frac{40}{35}\). Hence
  \(\frac{8}{7}\) is 40 copies of \(\frac{1}{35}\). Since \(40\div 5=8\), it
  follows that \(\frac{1}{5}\) of \(\frac{40}{35}\) is 8 copies of
  \(\frac{1}{35}\), or \(\frac{8}{35}\). Hence \(\frac{2}{5}\) of
  \(\frac{8}{7}\) is \(\frac{2\times 8}{5\times 7}=\frac{16}{35}\).
\end{ex}
\begin{ex}
  What is \(\frac{3}{17}\) of \(\frac{13}{15}\)?

  By equivalent fractions,
  \(\frac{13}{15}=\frac{17\times 13}{17\times 15}\). Since \((17\times 13)\div
  17=13\), by dividing \(\frac{13}{15}\) into 17 equal parts, we get segments
  of length \(\frac{13}{17\times 15}\). By concatenating 3 of these parts, we
  get a segment of length \(\frac{3\times 13}{17\times 15}\). Hence
  \(\frac{3}{17}\) of \(\frac{13}{15}\) is \(\frac{3\times 13}{15\times 17}\).
\end{ex}
\begin{thm}
  \label{k/l of m/n}
  If \(\frac{k}{l}\) and \(\frac{m}{n}\) are fractions, then
  \begin{equation}
     \frac{k}{l} \text{ of } \frac{m}{n} = \frac{km}{ln}.
  \end{equation}
\end{thm}
\begin{proof}
  By equivalent fractions, \(\frac{m}{n}=\frac{lm}{ln}\), which is of \(lm\)
  copies of \(\frac{1}{ln}\). Since \((lm)\div l=m\), \(\frac{1}{l}\) of
  \(\frac{lm}{ln}\) is \(m\) copies of \(\frac{1}{ln}\). By concatenating \(k\)
  parts, we get \(km\) copies of \(\frac{1}{ln}\), so \(\frac{k}{l}\) of
  \(\frac{lm}{ln}\) is \(\frac{km}{ln}\).
\end{proof}
\begin{ex}
  If a bird has flown \(\frac{3}{5}\) of the distance from its nest to a tree,
  and it has \SI{3/10}{\kilo\metre} to go, how far is it from the nest to the
  tree?

  To model this situation on the number line, imagine a line from the nest to
  the tree. Let 0 represent the position of the nest, 1 represent the point
  \SI{1}{\kilo\metre} away from the nest in the direction of the tree, and \(x\)
  represent the position of the tree. Divide \([0,x]\) into 5 equal parts. We
  know that the bird is at the third division point, so the distance from the
  current location of the bird to the tree is equal to the length of 2 parts. It
  is given that the length of 2 parts is \(\frac{3}{10}\). To get the length of
  1 part, we divide \(\frac{3}{10}\) into 2 equal parts, and then the total
  distance from the nest to the tree is the length of 5 parts. In other words,
  our problem has become finding \(\frac{5}{2}\) of \(\frac{3}{10}\). We shall
  repeat the proof of Theorem \ref{k/l of m/n}. By equivalent fractions,
  \(\frac{3}{10}=\frac{3\times 2}{10\times 2}\). Since \((3\times 2)\div 2=3\),
  the length of 1 part is \(\frac{3}{20}\). Hence the length of 5 parts is
  \(\frac{15}{20}\), and so the distance from the nest to the tree is
  \SI{3/4}{\kilo\metre}.
\end{ex}

\section{Adding fractions}
\label{sec:adding}
Consider the addition of whole numbers, for example, 2 and 3. We can think of
this as combining 2 things with 3 things. On the number line, we can think of
this as the length of the segement formed by joining 2 copies of 1 with 3 copies
of 1. We didn't really use anything special about the whole numbers, and we
might as well extend our definition to fractions. This leads to the following
definition:
\begin{defn}
  Let \(\frac{k}{l}\) and \(\frac{m}{n}\) be fractions. We define the addition,
  or sum of the two fractions, \(\frac{k}{l}+\frac{m}{n}\), by the length of the
  segment formed by concatenating a segment of length \(\frac{k}{l}\) with a
  segment of length \(\frac{m}{n}\).
\end{defn}
\begin{thm}[Commutativity]
  Let \(a,b\) be fractions. Then \(a+b=b+a\).
\end{thm}
\begin{thm}[Associativity]
  Let \(a,b,c\) be fractions. Then \(a+(b+c)=(a+b)+c\).
\end{thm}
From the definition, it is not obvious that the sum of any two fractions is a
fraction. However, consider the following example:
\begin{ex}
  What is \(\frac{2}{7}+\frac{3}{7}\)?

  Consider the sequence of 7ths. By concatenating 2 segments between consecutive
  points in the sequence of 7ths, we get a segment of length \(\frac{2}{7}\),
  and for 3 segments, we get a segment of length \(\frac{3}{7}\). Hence
  \(\frac{2}{7}+\frac{3}{7}\) has length equal to 5 concatenated segments. But
  by concatenating 5 segments, we get a segment of length \(\frac{5}{7}\), so
  \(\frac{2}{7}+\frac{3}{7}=\frac{5}{7}\).
\end{ex}
The same reason can be used to show: 
\begin{lem}
  \label{add common denom}
  Let \(\frac{k}{n}\) and \(\frac{l}{n}\) be fractions. Then
  \[\frac{k}{n}+\frac{l}{n}=\frac{k+l}{n}.\]
\end{lem}

Note that by using equivalent fractions, we may bring any two fractions under
the same denominator, and from above, it follows that the sum of two fractions
is also a fraction.
\begin{ex}
  What is \(\frac{5}{7}+\frac{3}{4}\)?

  Note that \(\frac{5}{7}=\frac{5\times 4}{7\times 4}=\frac{20}{28}\) and
  \(\frac{3}{4}=\frac{3\times 7}{4\times 7}=\frac{21}{28}\). Hence
  \(\frac{5}{7}+\frac{3}{4}=\frac{20}{28}+\frac{21}{28}=\frac{41}{28}\).
\end{ex}
In general, \(\frac{k}{l}+\frac{m}{n}=\frac{kn+ml}{ln}\), which is a fraction.
Notice that we didn't need to use the least common multiple (LCM) of two whole
numbers anywhere. However, using it can make computations easier, as shown in
the following example:
\begin{ex}
  What is \(\frac{3}{4}+\frac{5}{8}\)? Note that \(\frac{3}{4}=\frac{6}{8}\), so
  \(\frac{3}{4}+\frac{5}{8}=\frac{6}{8}+\frac{5}{8}=\frac{11}{8}\). If we didn't
  notice this, we could have done \(\frac{3}{4}+\frac{5}{8}=\frac{3\times
    8}{4\times 8}+\frac{5\times 4}{8\times 4}=\frac{44}{32}=\frac{11}{8}\).
\end{ex}
More generally, suppose \(c\) is a common multiple of \(l\) and \(n\), so for
some whole numbers \(a\) and \(b\), \(c=al=bn\). Then
\(\frac{k}{l}+\frac{m}{n}=\frac{ak}{al}+\frac{bm}{bn}=\frac{ak+bm}{c}\).
Consider the following example:
\begin{ex}
  What is \(\frac{11}{36}+\frac{27}{24}\)? Note that \(144=36\times 4=24\times
  6\), so
  \(\frac{11}{36}+\frac{27}{24}=\frac{44}{144}+\frac{162}{144}=\frac{206}{144}=\frac{103}{72}\).
  If we noted instead that the LCM of 36 and 24 is 72, we would have
  \(\frac{11}{36}+\frac{27}{24}=\frac{22}{72}+\frac{81}{72}=\frac{103}{72}\).
\end{ex}

\subsection{Adding finite decimals}
\label{sec:adding decimals}
What is \(3.1415+2.71\)? You might know the following algorithm:
\begin{enumerate}
\item If one number has fewer decimal places than the other, then pad that
  number with zeros to the right so that both numbers have the same number of
  decimal places. For example, \(2.71\) becomes \(2.7100\).
\item Line up the numbers by their decimal point.
  \[
    \begin{aligned}[c]
      &3.1415 \\
      &2.7100 \\
    \end{aligned}
  \]
\item Add the decimals as if they were whole numbers by ignoring the decimal point.
  \[
    \begin{aligned}[c]
      &31415 &  \\
      &27100 & +\\
      \hline
      &58515 &
    \end{aligned}
  \]
\item Insert the decimal point back. We get \(5.8515\).
\end{enumerate}
But why does it work?
Recall that decimals are fractions, and so
\[3.1415=\frac{31415}{10000} \quad\text{and}\quad 2.71=\frac{271}{100}.\]
 We know that
for two fractions with a common denominator, \(\frac{k}{n}\) and
\(\frac{l}{n}\), we have \[\frac{k}{n}+\frac{l}{n}=\frac{k+l}{n}.\]
To bring the fractions under a common denominator, we apply Lemma \ref{equivalent fractions}, so
\[2.71=\frac{271}{100}=\frac{27100}{10000}=2.7100.\] To find
\(\frac{31415}{10000}+\frac{27100}{10000}\), we will find \(31415+27100\). These
are the numbers we get by ignoring the decimal point. We insert the decimal
point back, because we are finding \(\frac{31415+27100}{10000}\), not just
\(31415+27100\).

\subsection{Complete expanded form of a finite decimal}
\label{sec:expanded form}
If we have a whole number 12345, its expanded form is the decomposition into the
sum of the place values of its digits: \[12345=(1\times 10^4)+(2\times
  10^3)+(3\times 10^2)+(4\times 10^1)+(5\times 10^0).\] If we have a decimal
1.2345, we have something similar. Using the expanded form of 12345, we have
\[  1.2345 = \frac{12345}{10^4} = \frac{(1\times 10^4)+(2\times 10^3)+(3\times 10^2)+(4\times 10^1)+(5\times 10^0)}{10^4}.\]
By Lemma \ref{add common denom} and Lemma \ref{equivalent fractions},
\begin{align*}
  1.2345 &= \frac{1\times 10^4}{10^4}+\frac{2\times 10^3}{10^4}+\frac{3\times
           10^2}{10^4} + \frac{4\times 10^1}{10^4} + \frac{5\times 10^0}{10^4}. \\
         &= 1 + \frac{2}{10^1} + \frac{3}{10^2} +
           \frac{4}{10^3} + \frac{5}{10^4} \\
         &= 1+0.2+0.03+0.004+0.0005.
\end{align*}
We call 0.2 the extended place value of 2 in \(1.2345\), and the decomposition
into the sum of the extended place values of its digits the complete expanded
form of \(1.2345.\)
\begin{rem}
  You may have encountered the definition of the decimal 1.2345 as ``1, and 2
  tenths, and 3 hundredths, and 4 thousandths, and 5 ten-thousandths''. This
  definition is just the complete expanded form of \(1.2345,\) defined as a
  fraction.
\end{rem}

\subsection{Mixed numbers}
A mixed number is a sum \(n+\frac{k}{l},\) where \(n\) is a whole number and
\(\frac{k}{l}\) is a proper fraction, denoted as \(n\frac{k}{l}\), not to be
confused with the product of \(n\) and \(\frac{k}{l}\).
\begin{ex}
  \[ \frac{84}{17} = \frac{4\times 17+16}{17} = \frac{4\times
      17}{17}+\frac{16}{17} = 4+\frac{16}{17} = 4\frac{16}{17}.\]
\end{ex}
\begin{rem}
  In later years, we don't use mixed numbers, and \(n\frac{k}{l}\) is to be
  interpreted only in the latter sense, as a product.
\end{rem}

% \section{Subtracting fractions}
% \label{sec:subtracting}
% Subtracting fractions is based on the subtraction of whole numbers. Consider
% some whole numbers, one bigger than the other, like 8 and 5. Then we can think
% of \(8-5\) by taking away 5 things from 8 things, leaving us with 3 things. On
% the number line, we take away 5 copies of 1 from 8 copies of 1, and then what
% remains is a segment of length \(8-5\). Now consider the fractions
% \(\frac{3}{4}\) and \(\frac{2}{3}\). Since \(\frac{3}{4}>\frac{2}{3},\) we can
% take away a segment of length \(\frac{2}{3}\) from a segment of length
% \(\frac{3}{4}\), and think of \(\frac{3}{4}-\frac{2}{3}\) as the length of the
% remaining segment. Since \(\frac{3}{4}=\frac{9}{12}\) and
% \(\frac{2}{3}=\frac{8}{12}\), \(\frac{3}{4}-\frac{2}{3}=\frac{9}{12}-\frac{8}{12}\).
\end{document}